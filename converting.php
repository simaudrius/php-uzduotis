<?php

class XMLConversion{

    function __construct($array) { 

$xmlDocument = new DOMDocument();
$root = $xmlDocument->appendChild($xmlDocument->createElement("people"));

foreach($array as $employee){
if(!empty($employee)){
$empRecord = $root->appendChild($xmlDocument->createElement('person'));
foreach($employee as $key=>$val){
$empRecord->appendChild($xmlDocument->createElement($key, $val));
}
}
}
$fileName = "people.xml";
$xmlDocument->formatOutput = true;
$xmlDocument->save($fileName);
    }
        
}

class CSVConcversion{
    function __construct($array)
    {
        $has_header = false;

  foreach ($array as $c) {

      $fp = fopen('people.csv', 'a');

      if (!$has_header) {
        fputcsv($fp, array_keys($c));
        $has_header = true;
      }

      fputcsv($fp, $c);
      fclose($fp);
    }
}
}

class JSONConversion{
    function __construct($array)
    {
        $fp = fopen('people.json', 'w');
        fwrite($fp, json_encode($array));
        fclose($fp);
    }
}

?>